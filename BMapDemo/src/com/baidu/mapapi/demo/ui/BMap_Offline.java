package com.baidu.mapapi.demo.ui;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKOLSearchRecord;
import com.baidu.mapapi.MKOLUpdateElement;
import com.baidu.mapapi.MKOfflineMap;
import com.baidu.mapapi.MKOfflineMapListener;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.R;

public class BMap_Offline extends MapActivity implements MKOfflineMapListener,
		OnClickListener {

	private MapView mMapView = null;
	private MKOfflineMap mOfflineMap = null;
	private EditText mEditCityName;
	private EditText mEditCityId;
	private TextView mText;

	private Button startButton;
	private Button stopButton;
	private Button delButton;
	private Button scanButton;
	private Button searchButton;
	private Button getButton;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_offline);
		setTitle("离线地图");

		initUI();

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();
		super.initMapActivity(app.mBMapManager);

		mOfflineMap = new MKOfflineMap();
		mOfflineMap.init(app.mBMapManager, this);

		ArrayList<MKOLUpdateElement> info = mOfflineMap.getAllUpdateInfo();
		if (info != null) {
			Log.d("andli", String.format("has %d city info", info.size()));
			if (info.get(0).status == MKOLUpdateElement.FINISHED) {

			}
		}

		ArrayList<MKOLSearchRecord> records = mOfflineMap.getHotCityList();
		if (records != null) {
			Log.d("andli", String.format("has %d hot city", records.size()));
		}

	}

	private void initUI() {
		mMapView = (MapView) findViewById(R.id.bmapView);
		mMapView.setBuiltInZoomControls(true);
		mEditCityName = (EditText) findViewById(R.id.city);
		mEditCityId = (EditText) findViewById(R.id.cityid);
		mText = (TextView) findViewById(R.id.text);

		startButton = (Button) findViewById(R.id.start);
		startButton.setOnClickListener(this);

		stopButton = (Button) findViewById(R.id.stop);
		stopButton.setOnClickListener(this);

		searchButton = (Button) findViewById(R.id.search);
		searchButton.setOnClickListener(this);

		delButton = (Button) findViewById(R.id.del);
		delButton.setOnClickListener(this);

		scanButton = (Button) findViewById(R.id.scan);
		scanButton.setOnClickListener(this);

		getButton = (Button) findViewById(R.id.get);
		getButton.setOnClickListener(this);

	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onGetOfflineMapState(int type, int state) {
		switch (type) {
		case MKOfflineMap.TYPE_DOWNLOAD_UPDATE: {
			Log.d("andli", String.format("cityid:%d update", state));
			MKOLUpdateElement update = mOfflineMap.getUpdateInfo(state);
			mText.setText(String.format("%s : %d%%", update.cityName,
					update.ratio));
		}
			break;
		case MKOfflineMap.TYPE_NEW_OFFLINE:
			Log.d("andli", String.format("add offlinemap num:%d", state));
			break;
		case MKOfflineMap.TYPE_VER_UPDATE:
			Log.d("andli", String.format("new offlinemap ver"));
			break;
		}

	}

	int cityid = -1;

	@Override
	public void onClick(View v) {
		try {
			cityid = Integer.parseInt(mEditCityId.getText().toString());
			if (v == startButton) {
				if (mOfflineMap.start(cityid)) {
					Log.d("andli", String.format("start cityid:%d", cityid));
				} else {
					Log.d("andli", String.format("not start cityid:%d", cityid));
				}

			} else if (v == delButton) {
				if (mOfflineMap.remove(cityid)) {
					Log.d("andli", String.format("del cityid:%d", cityid));
				} else {
					Log.d("andli", String.format("not del cityid:%d", cityid));
				}

			} else if (v == stopButton) {
				if (mOfflineMap.pause(cityid)) {
					Log.d("andli", String.format("stop cityid:%d", cityid));
				} else {
					Log.d("andli", String.format("not pause cityid:%d", cityid));
				}

			} else if (v == scanButton) {

				int num = mOfflineMap.scan();
				if (num != 0)
					mText.setText(String.format("已安装%d个离线包", num));
				Log.d("andli", String.format("scan offlinemap num:%d", num));

			} else if (v == searchButton) {
				search();
			} else if (v == getButton) {
				seeMap();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, e.toString(), 5000).show();

		}

	}

	private void seeMap() {
		MKOLUpdateElement element = mOfflineMap.getUpdateInfo(cityid);
		if (element != null) {
			String msg = String.format("大小:%.2fMB 已下载%d%%",
					((double) element.size) / 1000000, element.ratio);
			new AlertDialog.Builder(BMap_Offline.this)
					.setTitle(element.cityName)
					.setMessage(msg)
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int whichButton) {

								}
							}).show();
		} else {
			Toast.makeText(this, "NULL", 5000).show();
		}

	}

	private void search() {
		String cityName = mEditCityName.getText().toString();
		ArrayList<MKOLSearchRecord> res = mOfflineMap.searchCity(cityName);// 根据名称搜索
		if (res == null || res.size() != 1) {
			Toast.makeText(this, "空", 5000).show();
			return;
		}
		mEditCityId.setText(String.valueOf(res.get(0).cityID));

	}
}