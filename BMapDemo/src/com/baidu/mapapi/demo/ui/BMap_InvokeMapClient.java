/**
 * @author lilin
 * @date 2012-11-20 下午9:36:53
 * @Title: BMap_InvokeBMap.java
 * @Package com.baidu.mapapi.demo.ui.Overlay
 * @Description: TODO
 */

package com.baidu.mapapi.demo.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-20 下午9:59:08
 * @annotation 直接调用地图客户端
 */
public class BMap_InvokeMapClient extends Activity implements OnClickListener {

	private Button bmapButton;
	private Button gmapButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.bmap_invokebmap);
		setTitle("调用地图客户端");

		bmapButton = (Button) findViewById(R.id.bmapbutton);
		bmapButton.setOnClickListener(this);

		gmapButton = (Button) findViewById(R.id.gmapbutton);
		gmapButton.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		try {
			if (v == bmapButton) {
				 // 调用百度地图客户端
				 Intent intent = new Intent(Intent.ACTION_VIEW);
				 //调起百度PC或web地图，且在（39.916979519873，116.41004950566）坐标点上显示名称“我的位置”，内容“百度奎科大厦”的信息窗口。
				 Uri uri = Uri.parse( "http://api.map.baidu.com/marker?location=39.916979519873,116.41004950566&title=我的位置&content=百度奎科大厦");
				 intent.setData(uri);
				 intent.setPackage("com.baidu.BaiduMap");
				 startActivity(intent);
				
				// 调起百度地图客户端（Android）展示北京市的“银行”检索结果
				// intent://map/place/search?query=银行&region=北京&referer=yourCompanyName|yourAppName#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end
				// 调起百度地图客户端（Android）展示上海市“28”路公交车的检索结果
				// intent = Intent.getIntent("intent://map/line?coordtype=&zoom=&region=上海&name=28&referer=yourCompanyName|yourAppName#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
				// startActivity(intent); // 启动调用

			} else if (v == gmapButton) {
				// 调用Google地图客户端
				// Uri uri =
				// Uri.parse("http://ditu.google.cn/maps?hl=zh&mrt=loc&q=31.1198723,121.1099877(上海青浦大街100号)");
				double x = 31.88485336303711;
				double y = 120.55670166015625;
				String address = "城北新村31棟";
				Uri uri = Uri.parse("http://ditu.google.cn/maps?hl=zh&mrt=loc&q="+ x + "," + y + "(" + address + ")");
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW,uri);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK& Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
				intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
				startActivity(intent);

			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, e.toString(), 5000).show();
		}

	}
}
