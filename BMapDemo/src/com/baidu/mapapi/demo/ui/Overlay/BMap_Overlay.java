package com.baidu.mapapi.demo.ui.Overlay;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.Overlay;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-16 上午9:58:35
 * @annotation 覆盖物图层
 */
public class BMap_Overlay extends MapActivity {
	MapController mapController;
	MapView mapView;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_map);
		setTitle("覆盖物图层的简单使用");

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();
		this.initMapActivity(app.mBMapManager);// 初始化

		mapView = (MapView) findViewById(R.id.bmapView);
		mapController = mapView.getController();

		mapView.setBuiltInZoomControls(true);// 允许缩放操作
		mapView.getController().setZoom(12);// 设置缩放级别为12
		mapView.setDoubleClickZooming(true);// 允许双击放大
		// 添加覆盖物
		mapView.getOverlays().add(new MyOverlay());

	}

	// 自定义覆盖物
	public class MyOverlay extends Overlay {

		// 声明一个点
		private GeoPoint geoPoint = new GeoPoint((int) (39.915 * 1E6),
				(int) (116.404 * 1E6));

		private Paint paint = new Paint();

		@Override
		public void draw(Canvas canvas, MapView paramMapView,
				boolean paramBoolean) {
			super.draw(canvas, paramMapView, paramBoolean);

			Point point = mapView.getProjection().toPixels(geoPoint, null);
			canvas.drawText("*这里是天安门*", point.x, point.y, paint);
		}

	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager != null)
			app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	// @Override
	protected void onDestroy() {

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager != null) {
			app.mBMapManager.destroy();
			app.mBMapManager = null;
		}
		super.onDestroy();
	}
}
