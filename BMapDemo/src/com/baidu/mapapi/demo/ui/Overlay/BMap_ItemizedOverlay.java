package com.baidu.mapapi.demo.ui.Overlay;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.BMapHelp;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-16 下午12:52:57
 * @annotation 一组覆盖物
 */
public class BMap_ItemizedOverlay extends MapActivity {

	static View mPopView = null; // 点击mark时弹出的气泡View
	static MapView mMapView = null;
	int iZoom = 0;
	BMap_MyItemizedOverlay myItemizedOverlay = null;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_map);
		setTitle("一组覆盖物");

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();
		super.initMapActivity(app.mBMapManager);
		mMapView = (MapView) findViewById(R.id.bmapView);
		mMapView.setBuiltInZoomControls(true);

		mMapView.setDrawOverlayWhenZooming(true);// 设置在缩放动画过程中也显示overlay,默认为不绘制

		GeoPoint point = new GeoPoint((int) (39.90923 * 1e6),
				(int) (116.397428 * 1e6));
		mMapView.getController().setCenter(point);// 设置中心点
		iZoom = mMapView.getZoomLevel();// 获取缩放级别

		myItemizedOverlay = new BMap_MyItemizedOverlay(BMapHelp.setMarkImg(
				this, R.drawable.bmap_point_red), this, 3);
		mMapView.getOverlays().add(myItemizedOverlay); // 添加ItemizedOverlay实例到mMapView

		initPopView();// 创建点击mark时的弹出泡泡

	}

	private void initPopView() {
		mPopView = super.getLayoutInflater().inflate(R.layout.bmap_popview,
				null);
		mMapView.addView(mPopView, new MapView.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, null,
				MapView.LayoutParams.TOP_LEFT));
		mPopView.setVisibility(View.GONE);
	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager != null)
			app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}
