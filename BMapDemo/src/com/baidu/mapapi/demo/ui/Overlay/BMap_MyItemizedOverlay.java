package com.baidu.mapapi.demo.ui.Overlay;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.ItemizedOverlay;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.OverlayItem;
import com.baidu.mapapi.Projection;

/**
 * 
 * @author lilin
 * @date 2012-11-21 下午5:02:49
 * @annotation 点击覆盖物实现弹出式气泡
 */
public class BMap_MyItemizedOverlay extends ItemizedOverlay<OverlayItem> {

	public List<OverlayItem> itemList = new ArrayList<OverlayItem>();

	public List<GeoPoint> pList;

	public Drawable img;
	public Context con;

	private double mLat1 = 39.90923; // point1纬度
	private double mLon1 = 116.357428; // point1经度

	private double mLat2 = 39.90923;
	private double mLon2 = 116.397428;

	private double mLat3 = 39.90923;
	private double mLon3 = 116.437428;

	// 用与在地图上使用图片标识坐标
	BMap_MyItemizedOverlay(Drawable marker, Context context, int count) {
		super(boundCenterBottom(marker));

		this.img = marker;
		this.con = context;

		// 用给定的经纬度构造GeoPoint，单位是微度 (度 * 1E6)
		GeoPoint p1 = new GeoPoint((int) (mLat1 * 1E6), (int) (mLon1 * 1E6));
		GeoPoint p2 = new GeoPoint((int) (mLat2 * 1E6), (int) (mLon2 * 1E6));
		GeoPoint p3 = new GeoPoint((int) (mLat3 * 1E6), (int) (mLon3 * 1E6));

		// 构造OverlayItem的三个参数依次为：item的位置，标题文本，文字片段
		itemList.add(new OverlayItem(p1, "标题1", "文字片段1"));
		itemList.add(new OverlayItem(p2, "标题2", "文字片段2"));
		itemList.add(new OverlayItem(p3, "标题3", "文字片段3"));

		populate(); // 刷新地图
	}

	BMap_MyItemizedOverlay(Drawable marker, Context context, GeoPoint mGeoPoint) {
		super(boundCenterBottom(marker));

		this.img = marker;
		this.con = context;

		itemList.add(new OverlayItem(mGeoPoint, "", ""));

		populate(); // 刷新地图
	}

	BMap_MyItemizedOverlay(Drawable marker, Context context,
			List<GeoPoint> pList) {
		super(boundCenterBottom(marker));

		this.img = marker;
		this.con = context;
		for (int i = 0; i < pList.size(); i++) {
			itemList.add(new OverlayItem(pList.get(i), "", ""));
		}

		populate(); // 刷新地图
	}

	public void updateOverlay() {
		populate();
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {

		Projection projection = mapView.getProjection();// Projection接口用于屏幕像素坐标和经纬度坐标之间的变换

		for (int index = size() - 1; index >= 0; index--) { // 遍历mGeoList
			OverlayItem overLayItem = getItem(index); // 得到给定索引的item

			String title = overLayItem.getTitle();

			Point point = projection.toPixels(overLayItem.getPoint(), null);// 把经纬度变换到相对于MapView左上角的屏幕像素坐标

			// 可在此处添加您的绘制代码
			Paint mPaint = new Paint();
			mPaint.setColor(Color.BLUE);
			mPaint.setTextSize(15);
			canvas.drawText(title, point.x - 50, point.y, mPaint); // 绘制文本
		}

		super.draw(canvas, mapView, shadow);
		boundCenterBottom(img);// 调整一个drawable边界，使得（0，0）是这个drawable底部最后一行中心的一个像素
	}

	@Override
	protected OverlayItem createItem(int i) {
		return itemList.get(i);
	}

	@Override
	public int size() {
		return itemList.size();
	}

	@Override
	protected boolean onTap(int i) {// 处理当点击事件
		setFocus(itemList.get(i));// 获得焦点
		GeoPoint geoPoint = itemList.get(i).getPoint(); // 更新气泡位置,并使之显示
		LayoutParams params = new MapView.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, geoPoint,
				MapView.LayoutParams.BOTTOM_CENTER);
		BMap_ItemizedOverlay.mMapView.updateViewLayout(
				BMap_ItemizedOverlay.mPopView, params);
		BMap_ItemizedOverlay.mPopView.setVisibility(View.VISIBLE);
		// Toast.makeText(this.con, itemList.get(i).getSnippet(), 5000).show();
		return true;
	}

	@Override
	public boolean onTap(GeoPoint arg0, MapView arg1) {
		BMap_ItemizedOverlay.mPopView.setVisibility(View.GONE);// 消去弹出的气泡
		return super.onTap(arg0, arg1);
	}

}
