/**
 * @author lilin
 * @date 2012-11-20 下午5:16:32
 * @Title: BMapHelp.java
 * @Package com.baidu.mapapi.demo.ui
 * @Description: TODO
 */

package com.baidu.mapapi.demo;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Toast;

import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKGeocoderAddressComponent;
import com.baidu.mapapi.MKPoiInfo;
import com.baidu.mapapi.MapView;

/**
 * @author AndLi
 * 
 */
public class BMapHelp {

	// poi类型，0：普通点，1：公交站，2：公交线路，3：地铁站，4：地铁线路,

	// 通过对话框显示搜索结果
	public static void showPoiByDialog(Context context, List<MKPoiInfo> _poiList) {

		StringBuffer sb = new StringBuffer();

		int i = 1;
		for (MKPoiInfo mkPoiInfo : _poiList) {
			sb.append((i++) + mkPoiInfo.name).append("\n");

		}

		new AlertDialog.Builder(context)
				.setTitle("搜索到" + _poiList.size() + "条POI信息")
				.setMessage(sb.toString())
				.setPositiveButton("关闭", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				}).create().show();

	}

	public static void showPoiByDialog(Context context,
			ArrayList<MKPoiInfo> _poiList) {

		StringBuffer sb = new StringBuffer();

		int i = 1;
		for (MKPoiInfo mkPoiInfo : _poiList) {
			sb.append("----------------------------").append("\n");
			sb.append((i++) + "name:" + mkPoiInfo.name).append("\n");
			sb.append("address:" + mkPoiInfo.address).append("\n");
			sb.append("phoneNum:" + mkPoiInfo.phoneNum).append("\n");
			sb.append("postCode:" + mkPoiInfo.postCode).append("\n");
			sb.append("x:" + mkPoiInfo.pt.getLatitudeE6()).append("\n");
			sb.append("y:" + mkPoiInfo.pt.getLatitudeE6()).append("\n");
		}

		new AlertDialog.Builder(context)
				.setTitle("搜索到" + _poiList.size() + "条POI信息")
				.setMessage(sb.toString())
				.setPositiveButton("关闭", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				}).create().show();

	}

	public static void showPoiByDialog(Context context, MKPoiInfo poiInfo) {

		StringBuffer sb = new StringBuffer();

		sb.append("city=" + poiInfo.city).append("\n");
		sb.append("address=" + poiInfo.address).append("\n");
		sb.append("name=" + poiInfo.name).append("\n");
		sb.append("ePoiType=" + poiInfo.ePoiType).append("\n");
		sb.append("postCode=" + poiInfo.postCode).append("\n");
		sb.append("phoneNum=" + poiInfo.phoneNum).append("\n");
		GeoPoint geoPoint = poiInfo.pt;

		sb.append("bx=" + geoPoint.getLatitudeE6()).append("\n");
		sb.append("by=" + geoPoint.getLongitudeE6()).append("\n");
		sb.append("gx=" + (geoPoint.getLatitudeE6() / 1e6)).append("\n");
		sb.append("gy=" + (geoPoint.getLongitudeE6() / 1e6)).append("\n");

		Log.i("andli", ">>>>>   --------------------------");
		Log.i("andli", ">>>>>   city=" + poiInfo.city);
		Log.i("andli", ">>>>>   address=" + poiInfo.address);
		Log.i("andli", ">>>>>   ePoiType(POI类型)=" + poiInfo.ePoiType);
		Log.i("andli", ">>>>>   name=" + poiInfo.name);
		Log.i("andli", ">>>>>   uid=" + poiInfo.uid);
		Log.i("andli", ">>>>>   postCode(邮编)=" + poiInfo.postCode);
		Log.i("andli", ">>>>>   phoneNum=" + poiInfo.phoneNum);
		Log.i("andli", ">>>>>   x=" + poiInfo.pt.getLatitudeE6());
		Log.i("andli", ">>>>>   y=" + poiInfo.pt.getLongitudeE6());
		Log.i("andli", ">>>>>   --------------------------");

		new AlertDialog.Builder(context).setTitle("POI详情")
				.setMessage(sb.toString())
				.setPositiveButton("关闭", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
					}
				}).create().show();

	}

	public static void printPoiInfo(Context context, MKPoiInfo poiInfo) {

		Log.i("andli", ">>>>>   --------------------------");
		Log.i("andli", ">>>>>   city=" + poiInfo.city);
		Log.i("andli", ">>>>>   address=" + poiInfo.address);
		Log.i("andli", ">>>>>   ePoiType(POI类型)=" + poiInfo.ePoiType);
		Log.i("andli", ">>>>>   name=" + poiInfo.name);
		Log.i("andli", ">>>>>   uid=" + poiInfo.uid);
		Log.i("andli", ">>>>>   postCode(邮编)=" + poiInfo.postCode);
		Log.i("andli", ">>>>>   phoneNum=" + poiInfo.phoneNum);
		Log.i("andli", ">>>>>   x=" + poiInfo.pt.getLatitudeE6());
		Log.i("andli", ">>>>>   y=" + poiInfo.pt.getLongitudeE6());
		Log.i("andli", ">>>>>   --------------------------");

	}

	public static void printPoiInfo(Context context, MKAddrInfo poiInfo) {

		Log.i("andli", ">>>>>   --------------------------");
		Log.i("andli", ">>>>>   strAddr=" + poiInfo.strAddr);
		Log.i("andli", ">>>>>   strBusiness=" + poiInfo.strBusiness);
		Log.i("andli", ">>>>>   ePoiType(POI类型)=" + poiInfo.type);
		Log.i("andli", ">>>>>   name=" + poiInfo.addressComponents);
		printPoint(">>>>>   坐标=", poiInfo.geoPt);
		Log.i("andli", ">>>>>   --------------------------");

	}

	public static void showMKPoiInfo(MKPoiInfo mMkPoiInfo) {
		Log.i("andli", ">>>>>   --------------------------");
		Log.i("andli", ">>>>>   city=" + mMkPoiInfo.city);
		Log.i("andli", ">>>>>   address=" + mMkPoiInfo.address);
		Log.i("andli", ">>>>>   ePoiType(POI类型)=" + mMkPoiInfo.ePoiType);
		Log.i("andli", ">>>>>   name=" + mMkPoiInfo.name);
		Log.i("andli", ">>>>>   uid=" + mMkPoiInfo.uid);
		Log.i("andli", ">>>>>   postCode(邮编)=" + mMkPoiInfo.postCode);
		Log.i("andli", ">>>>>   phoneNum=" + mMkPoiInfo.phoneNum);
		Log.i("andli", ">>>>>   x=" + mMkPoiInfo.pt.getLatitudeE6());
		Log.i("andli", ">>>>>   y=" + mMkPoiInfo.pt.getLongitudeE6());
		Log.i("andli", ">>>>>   --------------------------");

	}

	public static void showMKAddrInfo(MKAddrInfo mMkPoiInfo) {
		Log.i("andli", ">>>>>   ==========================");
		Log.i("andli", ">>>>>   strAddr(地址名称)=" + mMkPoiInfo.strAddr);
		Log.i("andli", ">>>>>   strBusiness(商圈名称)=" + mMkPoiInfo.strBusiness);
		Log.i("andli", ">>>>>   type=" + mMkPoiInfo.type);
		// MKAddrInfo.MK_GEOCODE - 地理编码，由街道名称转换为坐标值
		// MKAddrInfo.MK_REVERSEGEOCODE - 反地理编码，由坐标转换为街道名称
		Log.i("andli", ">>>>>   MK_GEOCODE(地理编码)=" + MKAddrInfo.MK_GEOCODE);
		Log.i("andli", ">>>>>   MK_REVERSEGEOCODE(反地理编码)="
				+ MKAddrInfo.MK_REVERSEGEOCODE);
		printPoint(">>>>>   坐标", mMkPoiInfo.geoPt);
		Log.i("andli", ">>>>>   ==========================");

	}

	public static void printPoint(String title, GeoPoint point) {
		double x = point.getLatitudeE6();
		double y = point.getLongitudeE6();
		if (title != null && !title.equals("")) {
			Log.i("andli", ">>>>>   " + title + "(" + x + "," + y + ")");
		} else {
			Log.i("andli", ">>>>>   (" + x + "," + y + ")");
		}
	}

	public static void printXY(String title, double x, double y) {
		if (title != null && !title.equals("")) {
			Log.i("andli", ">>>>>   " + title + "(" + x + "," + y + ")");
		} else {
			Log.i("andli", ">>>>>   (" + x + "," + y + ")");
		}
	}

	public static void showPoint(Context con, GeoPoint point) {
		double x = point.getLatitudeE6();
		double y = point.getLongitudeE6();
		Log.i("andli", ">>>>>   (" + x + "," + y + ")");
		Toast.makeText(con, "(" + x + "," + y + ")", 5000).show();
	}

	// GPS坐标转百度坐标
	public static GeoPoint gpsToGeoPoint(double x, double y) {
		return new GeoPoint((int) (x * 1e6), (int) (y * 1e6));
	}

	public static Drawable setMarkImg(Context con, int id) {
		// 设置标记图标
		Drawable mark = con.getResources().getDrawable(id);
		int w = mark.getIntrinsicWidth();
		int h = mark.getIntrinsicHeight();
		mark.setBounds(0, 0, w, h); // 为maker定义位置和边界
		return mark;
	}

	/**
	 * @author lilin
	 * @date 2012-11-27 下午5:01:01
	 * @annotation 发大到最大比例
	 */
	public static void zoomTOBig(MapView mapView) {
		Log.i("andli", "缩放比例(老)=" + mapView.getZoomLevel());
		int num = mapView.getMaxZoomLevel() - mapView.getMaxZoomLevel();
		for (int i = 0; i < num - 3; i++) {
			mapView.getController().zoomIn();
		}
		Log.i("andli", "缩放比例(新)=" + mapView.getZoomLevel());
		Log.i("andli", "最大缩放比例=" + mapView.getMaxZoomLevel());
		Log.i("andli", "最小缩放比例=" + mapView.getMinZoomLevel());

	}

	public static void printMKGeocoderAddressComponent(
			MKGeocoderAddressComponent component) {
		// city
		// 城市名称
		// district
		// 区县名称
		// province
		// 省份名称
		// street
		// 街道名称
		// streetNumber
		// 门牌号码
		Log.i("andli", ">>>>>   ==========================");
		Log.i("andli", ">>>>>   city(城市名称)=" + component.city);
		Log.i("andli", ">>>>>   district(区县名称)=" + component.district);
		Log.i("andli", ">>>>>   province(城市名称)=" + component.province);
		Log.i("andli", ">>>>>   street(街道名称)=" + component.street);
		Log.i("andli", ">>>>>   streetNumber(门牌号码)=" + component.streetNumber);
		Log.i("andli", ">>>>>   ==========================");
	}

	// 计算2点之间的距离
	// public void calculateDistance() {
	// double result = 0.0;
	// if (markList.size() >= 2) {
	// for (int i = 1; i < markList.size(); i++) {
	// double distance = 0.0;
	// GeoPoint geoPoint1 = markList.get(i - 1).getPoint();
	// GeoPoint geoPoint2 = markList.get(i).getPoint();
	// double lat1 = geoPoint1.getLatitudeE6() / 1e6;
	// double lat2 = geoPoint2.getLatitudeE6() / 1e6;
	// double lon1 = geoPoint1.getLongitudeE6() / 1e6;
	// double lon2 = geoPoint2.getLongitudeE6() / 1e6;
	// double dLat = (lat2 - lat1) * Math.PI / 180;
	// double dLon = (lon2 - lon1) * Math.PI / 180;
	// double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
	// + Math.cos(lat1 * Math.PI / 180)
	// * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2)
	// * Math.sin(dLon / 2);
	// distance = (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)))
	// * EARTH_RADIUS;
	// result += distance;
	// }
	// }
	// distanceListener.onDistanceResult(result);
	// }

	private static double EARTH_RADIUS = 6378.137;// 地球半径

	/**
	 * 
	 * @author lilin
	 * @date 2012-11-28 下午3:04:20
	 * @annotation 计算2点间的距离
	 */
	public static String calculate2Distance(GeoPoint p1, GeoPoint p2) {
		double result = 0.0;
		double distance = 0.0;
		double lat1 = p1.getLatitudeE6() / 1e6;
		double lat2 = p2.getLatitudeE6() / 1e6;
		double lon1 = p1.getLongitudeE6() / 1e6;
		double lon2 = p2.getLongitudeE6() / 1e6;
		double dLat = (lat2 - lat1) * Math.PI / 180;
		double dLon = (lon2 - lon1) * Math.PI / 180;
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(lat1 * Math.PI / 180)
				* Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2)
				* Math.sin(dLon / 2);
		distance = (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)))
				* EARTH_RADIUS;
		result += distance;
		String resultString = "公里";
		if (result < 1) {
			resultString = ((double) ((int) Math.round(result * 1000 * 10)) / 10.0)
					+ "米";
		} else {
			resultString = ((double) (((int) Math.round(result * 10))) / 10.0)
					+ "公里";
		}
		return resultString;
		// distanceListener.onDistanceResult(result);
	}
}
