package com.android.smdonkeyditu;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.PoiOverlay;
import android.view.View;
import android.view.View.OnClickListener;
import com.baidu.mapapi.MapView;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.widget.EditText;
import com.baidu.mapapi.ItemizedOverlay;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.baidu.mapapi.OverlayItem;

public class GeoCodeSeacher extends Activity {
	Button mBtnReverseGeoCode = null;	// 将坐标反编码为地址
	Button mBtnGeoCode = null;	// 将地址编码为坐标
	Button mBtnSearch=null;
	boolean isSatelite = false;	// 
	MKSearch mSearch = null;	// 搜索模块，也可去掉地图模块独立使用	
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
        setContentView(R.layout.geocoder); 
        
        // 初始化搜索模块，注册事件监听
        mSearch = new MKSearch();
        mSearch.init(smdonkeyditu.mBMapMan, new MKSearchListener(){
			public void onGetAddrResult(MKAddrInfo res, int error) {
				if (error != 0) {
					String str = String.format("错误号：%d", error);
					Toast.makeText(GeoCodeSeacher.this, str, Toast.LENGTH_LONG).show();
					return;
				}
				smdonkeyditu.mMapView.getController().animateTo(res.geoPt);					
				String strInfo = String.format("纬度：%f 经度：%f\r\n", res.geoPt.getLatitudeE6()/1e6, 
							res.geoPt.getLongitudeE6()/1e6);

				Toast.makeText(GeoCodeSeacher.this, strInfo, Toast.LENGTH_LONG).show();
				Drawable marker = getResources().getDrawable(R.drawable.donkeysmall);  //得到需要标在地图上的资源
				marker.setBounds(0, 0, marker.getIntrinsicWidth(), marker
						.getIntrinsicHeight());   //为maker定义位置和边界
				smdonkeyditu.mMapView.getOverlays().clear();
				smdonkeyditu.mMapView.getOverlays().add(new OverItemT(marker, GeoCodeSeacher.this, res.geoPt, res.strAddr));
			}
			public void onGetPoiResult(MKPoiResult res, int type, int error) {
				// 错误号可参考MKEvent中的定义
				if (error != 0 || res == null) {
					Toast.makeText(GeoCodeSeacher.this, "抱歉，未找到结果", Toast.LENGTH_LONG).show();
					return;
				}

			    // 将地图移动到第一个POI中心点
			    if (res.getCurrentNumPois() > 0) {
				    // 将POI结果显示到地图上
					PoiOverlay poiOverlay = new PoiOverlay(GeoCodeSeacher.this, smdonkeyditu.mMapView);
					poiOverlay.setData(res.getAllPoi());
					smdonkeyditu.mMapView.getOverlays().clear();
					smdonkeyditu.mMapView.getOverlays().add(poiOverlay);
					smdonkeyditu.mMapView.invalidate();
					smdonkeyditu.mMapView.getController().animateTo(res.getPoi(0).pt);
			    } else if (res.getCityListNum() > 0) {
			    	String strInfo = "在";
			    	for (int i = 0; i < res.getCityListNum(); i++) {
			    		strInfo += res.getCityListInfo(i).city;
			    		strInfo += ",";
			    	}
			    	strInfo += "找到结果";
					Toast.makeText(GeoCodeSeacher.this, strInfo, Toast.LENGTH_LONG).show();
			    }
			}
			
			public void onGetDrivingRouteResult(MKDrivingRouteResult res,
					int error) {
			}
			public void onGetTransitRouteResult(MKTransitRouteResult res,
					int error) {
			}
			public void onGetWalkingRouteResult(MKWalkingRouteResult res,
					int error) {
			}
			public void onGetBusDetailResult(MKBusLineResult result, int iError) {
			}
			@Override
			public void onGetSuggestionResult(MKSuggestionResult res, int arg1) {
				// TODO Auto-generated method stub				
			}
        });
        // 设定地理编码及反地理编码按钮的响应
        mBtnReverseGeoCode = (Button)findViewById(R.id.reversegeocode);
        mBtnGeoCode = (Button)findViewById(R.id.geocode);        
        OnClickListener clickListener = new OnClickListener(){
			public void onClick(View v) {
				if (mBtnReverseGeoCode.equals(v)) {					
					finish();
					Toast.makeText(GeoCodeSeacher.this, "Goodbye!", Toast.LENGTH_LONG).show();
					
				} else if (mBtnGeoCode.equals(v)) {
					finish();					
					//Toast.makeText(GeoCodeSeacher.this, "Hello MyFather!", Toast.LENGTH_LONG).show();
					EditText editCity = (EditText)findViewById(R.id.city);
					EditText editGeoCodeKey = (EditText)findViewById(R.id.geocodekey);
					mSearch.geocode(editGeoCodeKey.getText().toString(), editCity.getText().toString());

				}					
			}
        };        
        mBtnReverseGeoCode.setOnClickListener(clickListener); 
        mBtnGeoCode.setOnClickListener(clickListener); 
        
        mBtnSearch = (Button)findViewById(R.id.search);
       
        OnClickListener clickListener1 = new OnClickListener(){
			public void onClick(View v) {
				 if (mBtnSearch.equals(v)) {
					EditText editCity = (EditText)findViewById(R.id.city);		
					EditText editSearchKey = (EditText)findViewById(R.id.geosearchkey);
					mSearch.poiSearchInCity(editCity.getText().toString(), 
							editSearchKey.getText().toString());
					finish();
					//smdonkeyditu.mMapView.bringToFront();
				}					
			}
        };        
        mBtnSearch.setOnClickListener(clickListener1); 
        
	}
	
	public class OverItemT extends ItemizedOverlay<OverlayItem>{
		private List<OverlayItem> mGeoList = new ArrayList<OverlayItem>();

		public OverItemT(Drawable marker, Context context, GeoPoint pt, String title) {
			super(boundCenterBottom(marker));
			
			mGeoList.add(new OverlayItem(pt, title, null));

			populate();
		}

		@Override
		protected OverlayItem createItem(int i) {
			return mGeoList.get(i);
		}

		@Override
		public int size() {
			return mGeoList.size();
		}

		@Override
		public boolean onSnapToItem(int i, int j, Point point, MapView mapview) {
			Log.e("ItemizedOverlayDemo","enter onSnapToItem()!");
			return false;
		}
	}
}	


