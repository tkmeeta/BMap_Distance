/*
 * 文件名: Test.java
 * 版    权：  Copyright DingliCom Tech. Co. Ltd. All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 黄广府
 * 创建时间:2012-6-5
 * 
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap.ui;

import com.dinglicom.walktourmap.R;

import android.app.Activity;
import android.os.Bundle;

/**
 * [一句话功能简述]<BR>
 * [功能详细描述]
 * @author 黄广府
 * @version [WalkTour Client V100R001C03, 2012-6-5] 
 */
public class Test extends Activity{

    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param savedInstanceState
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
    }
    
    
}
