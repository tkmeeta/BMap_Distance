/*
 * 文件名: DownloadOfflineMapActivity.java
 * 版    权：  Copyright DingliCom Tech. Co. Ltd. All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 黄广府
 * 创建时间:2012-6-1
 * 
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.baidu.mapapi.MKOLSearchRecord;
import com.baidu.mapapi.MKOLUpdateElement;
import com.baidu.mapapi.MKOfflineMap;
import com.baidu.mapapi.MKOfflineMapListener;
import com.dinglicom.walktourmap.Logger;
import com.dinglicom.walktourmap.R;
import com.dinglicom.walktourmap.logic.adapter.DownloadMapAdapter;

/**
 * [一句话功能简述]<BR>
 * [功能详细描述]
 * @author 黄广府
 * @version [WalkTour Client V100R001C03, 2012-6-1] 
 */
public class DownloadMapActivity extends Activity implements OnClickListener,
        MKOfflineMapListener, OnItemClickListener {
    
    public static String TAG = "DownloadMapActivity";
    
    /**
     * 离线地图管理对象
     */
    private MKOfflineMap mkOfflineMap;
    
    /**
     * 负责显示当前城市ListView
     */
    private ListView currentCityListView;
    
    /**
     * 负责显示热门城市ListView
     */
    private ListView hotCityListView;
    
    /**
     * 负责显示全国城市ListView
     */
    private ListView allCityListView;
    
    /**
     * 热门城市地图列表
     */
    private List<MKOLSearchRecord> hotCityList;
    
    /**
     * 全国城市地图列表
     */
    private List<MKOLSearchRecord> allCityList;
    
    /**
     * GE
     */
    private HashMap<Integer, MKOLUpdateElement> updateElementHashMap;
    
    private ListAdapter hotCityListAdapter;
    
    private ListAdapter allCityListAdapter;
    
    private ProgressBar progressbar;
    
    private final int REFRESH_LIST = 101;
    
    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:
                    hotCityListView.setAdapter(hotCityListAdapter);
                    allCityListView.setAdapter(allCityListAdapter);
                    new Thread(new SetListViewHeightBasedOnChildren()).start();
                    break;
                default:
                    break;
            }
            return true;
        }
    });
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param savedInstanceState
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offsearch);
        findView();
        initData();
    }
    
    /**
     * 初始化VIEW控件<BR>
     * [功能详细描述]
     */
    public void findView() {
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        currentCityListView = (ListView) findViewById(R.id.ListView_off_search_curcity);
        hotCityListView = (ListView) findViewById(R.id.ListView_off_search_hotcity);
        allCityListView = (ListView) findViewById(R.id.ListView_off_search_search);
    }
    
    /**
     * 初始化数据<BR>
     * [功能详细描述]
     */
    public void initData() {
        mkOfflineMap = new MKOfflineMap();
        //mkOfflineMap.init(WalktourApp.getContext().mMapManager, this);
        new Thread(new GetAllUpdateInfo()).start();
    }
    
    
    /**
     * ListView处理<BR>
     * 该方法解决ScroolView 中嵌套ListView的问题
     * 设置完ListView的Adapter后，根据ListView的子项目重新计算ListView的高度，
     * 然后把高度再作为LayoutParams设置给ListView
     */
    public class SetListViewHeightBasedOnChildren implements Runnable{

        @Override
        public void run() {
            ListAdapter hotlistAdapter = hotCityListView.getAdapter();
            if (hotlistAdapter == null) {
                return;
            }
            int hottotalHeight = 0;
            for (int i = 0; i < hotlistAdapter.getCount(); i++) {
                View listItem = hotlistAdapter.getView(i, null, hotCityListView);
                listItem.measure(0, 0);
                hottotalHeight += listItem.getMeasuredHeight();
            }
            final ViewGroup.LayoutParams hotparams = hotCityListView.getLayoutParams();
            hotparams.height = hottotalHeight
                    + (hotCityListView.getDividerHeight() * (hotlistAdapter.getCount() - 1));
            
            
            
            ListAdapter alllistAdapter = allCityListView.getAdapter();
            if (alllistAdapter == null) {
                return;
            }
            int alltotalHeight = 0;
            for (int i = 0; i < alllistAdapter.getCount(); i++) {
                View listItem = alllistAdapter.getView(i, null, allCityListView);
                listItem.measure(0, 0);
                alltotalHeight += listItem.getMeasuredHeight();
            }
            final ViewGroup.LayoutParams allparams = allCityListView.getLayoutParams();
            allparams.height = alltotalHeight
                    + (allCityListView.getDividerHeight() * (alllistAdapter.getCount() - 1));
            
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hotCityListView.setLayoutParams(hotparams);
                    allCityListView.setLayoutParams(allparams);
                    progressbar.setVisibility(View.GONE); 
                }
            });
        }
    }
    
    /**
     * 获取地图下载更新信息<BR>
     * [功能详细描述]
     * @author 黄广府
     * @version [WalkTour Client V100R001C03, 2012-6-4]
     */
    class GetAllUpdateInfo implements Runnable {
        /**
         * [一句话功能简述]<BR>
         * [功能详细描述]
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            hotCityList = mkOfflineMap.getHotCityList();
            allCityList = mkOfflineMap.getOfflineCityList();
            ArrayList<MKOLUpdateElement> updateElementList = mkOfflineMap.getAllUpdateInfo();
            updateElementHashMap = new HashMap<Integer, MKOLUpdateElement>();
            if(updateElementList != null){
                for (MKOLUpdateElement element : updateElementList) {
                    updateElementHashMap.put(element.cityID, element);
                }
            }
            if(hotCityList == null){
                hotCityList = new ArrayList<MKOLSearchRecord>();
            }
            if(allCityList == null){
                allCityList = new ArrayList<MKOLSearchRecord>();
            }
            hotCityListAdapter = new DownloadMapAdapter(DownloadMapActivity.this, mkOfflineMap,
                    hotCityList,updateElementHashMap);
            allCityListAdapter = new DownloadMapAdapter(DownloadMapActivity.this, mkOfflineMap,
                    allCityList,updateElementHashMap);
            Message message = new Message();
            message.what = REFRESH_LIST;
            mHandler.sendMessage(message);
        }
    }
    
    
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        //WalktourApp.getContext().mMapManager.start();
        super.onResume();
    }

    
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        //WalktourApp.getContext().mMapManager.stop();
        super.onDestroy();
    }

    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param v
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param arg0
     * @param arg1
     * @see com.baidu.mapapi.MKOfflineMapListener#onGetOfflineMapState(int, int)
     */
    @Override
    public void onGetOfflineMapState(int type, int state) {
        Logger.e(TAG, "onGetOfflineMapState:" + "type:" + type + "    state:"
                + state);
        switch (type) {
            case MKOfflineMap.TYPE_DOWNLOAD_UPDATE:
/*                MKOLUpdateElement element = mkOfflineMap.getUpdateInfo(state);
                if(MKOLUpdateElement.FINISHED == element.status){
                    updateElementHashMap.put(element.cityID, element);
                    Message message = new Message();
                    message.what = REFRESH_LIST;
                    mHandler.sendMessage(message);
                }
                Logger.e(TAG, "cityName:" + element.cityName + "    \n服务端数据大小:"
                        + element.serversize / 1024 / 1024 + "    \n百分比："
                        + element.ratio + "\n下载状态：" + element.status);
                Toast.makeText(DownloadMapActivity.this,
                        "cityName:"+element.cityName+    "\n服务端数据大小:"+element.serversize/1024/1024+ "  \n百分比："+element.ratio  + "\n下载状态：" +element.status,
                        Toast.LENGTH_LONG)
                        .show();*/
                break;
            
            default:
                break;
        }
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param arg0
     * @param itemView
     * @param arg2
     * @param arg3
     * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
     */
    @Override
    public void onItemClick(AdapterView<?> arg0, View itemView, int arg2,
            long arg3) {
        switch (arg0.getId()) {
            case R.id.ListView_off_search_hotcity:
                break;
            case R.id.ListView_off_search_search:
                
                break;
            default:
                break;
        }
        
    }
    
}
