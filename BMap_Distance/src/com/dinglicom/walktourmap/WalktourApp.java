/*
 * 文件名: WalktouApp.java
 * 版    权：  Copyright DingliCom Tech. Co. Ltd. All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 黄广府
 * 创建时间:2012-5-29
 * 
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap;

import java.io.IOException;

import android.app.Application;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKEvent;
import com.baidu.mapapi.MKGeneralListener;

/**
 * [一句话功能简述]<BR>
 * [功能详细描述]
 * @author 黄广府
 * @version [WalkTour Client V100R001C03, 2012-5-29] 
 */
public class WalktourApp extends Application {
    
    /**
     * 应用全局context对象
     */
    private static WalktourApp context;
    
    /**
     * SDK版本号
     */
    private static int sdkVersion;
    
    /**
     * 百度MapAPI的管理类
     */
    public BMapManager mMapManager = null;
    
    /**
     * 百度地图KEY
     */
    public String mapKey = "333C0A15D54AC6775A0646E1980CABE6B7096029";
    
    boolean m_bKeyRight = true; // 授权Key正确，验证通过
    
    public WalktourApp() {
        super();
        context = this;
    }
    
    @Override
    public void onCreate() {
        Logger.d("BMapApiDemoApp", "onCreate");
        context = this;
        mMapManager = new BMapManager(this);
        mMapManager.init(this.mapKey, new MyGeneralListener());
        mMapManager.getLocationManager().setNotifyInternal(10, 5);
        super.onCreate();
/*        Runtime runtime = Runtime.getRuntime();
        try {
            Process proc = runtime.exec("su");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }
    
    /**
     * 常用事件监听，用来处理通常的网络错误，授权验证错误等<BR>
     * [功能详细描述]
     * @author 黄广府
     * @version [WalkTour Client V100R001C03, 2012-5-29]
     */
    public static class MyGeneralListener implements MKGeneralListener {
        @Override
        public void onGetNetworkState(int iError) {
            Log.d("MyGeneralListener", "onGetNetworkState error is " + iError);
            Toast.makeText(WalktourApp.context.getApplicationContext(),
                    "您的网络出错啦！",
                    Toast.LENGTH_LONG).show();
        }
        
        @Override
        public void onGetPermissionState(int iError) {
            Log.d("MyGeneralListener", "onGetPermissionState error is "
                    + iError);
            if (iError == MKEvent.ERROR_PERMISSION_DENIED) {
                // 授权Key错误：
                Toast.makeText(WalktourApp.context.getApplicationContext(),
                        "请在BMapApiDemoApp.java文件输入正确的授权Key！",
                        Toast.LENGTH_LONG).show();
                WalktourApp.context.m_bKeyRight = false;
            }
        }
    }
    
    public static WalktourApp getContext() {
        return context;
    }
    
    /**
     * 获得版本号<BR>
     * [功能详细描述]
     * @return SDK版本号
     */
    public static int getSDKVersion() {
        if (WalktourApp.sdkVersion == 0) {
            WalktourApp.sdkVersion = Integer.parseInt(Build.VERSION.SDK);
        }
        return WalktourApp.sdkVersion;
    }
    
    @Override
    /**
     * 建议在您app的退出之前调用mapadpi的destroy()函数，避免重复初始化带来的时间消耗<BR>
     * [功能详细描述]
     * @see android.app.Application#onTerminate()
     */
    public void onTerminate() {
        if (mMapManager != null) {
            mMapManager.destroy();
            mMapManager = null;
        }
        super.onTerminate();
    }
}
