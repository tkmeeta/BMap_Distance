/*
 * 文件名: BaseContactUtil.java
 * 版    权：  Copyright Huawei Tech. Co. Ltd. All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 杨凡
 * 创建时间:Feb 11, 2012
 * 
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap.utils;

import java.io.Serializable;
import java.text.CollationKey;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.util.Log;

import com.dinglicom.walktourmap.logic.model.City;

/**
 * 联系人UI显示工具类<BR>
 * 提供排序、获取首字母等方法
 * 
 * @author 黄广府
 * @version [RCS Client V100R001C03, 2011-10-17]
 */
public class BaseContactUtil {
    
    /**
     * debug tag
     */
    private static final String TAG = "BaseContactUtil";

    
    /**
     * 比较器<BR>
     * 
     * @author 黄广府
     * @version [RCS Client V100R001C03, 2011-10-17]
     */
    public static class BaseCityComparator implements
            Comparator<City>, Serializable {
        
        /**
         * 序列化ID
         */
        private static final long serialVersionUID = 1L;
        
        /**
         * 
         * 比较函数<BR>
         * [功能详细描述]
         * 
         * @param cityOne BaseContactModel
         * @param cityTwo BaseContactModel
         * @return a negative value if contactOne is less than contactTwo, 0 if
         *         they are equal and a positive value if contactOne is greater
         *         than contactTwo.
         * 
         * @see java.util.Comparator#
         * compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(City cityOne,
                City cityTwo) {
            Collator cmp = Collator.getInstance(java.util.Locale.CHINA);
            String pyOne = cityOne.getSimplePinyin() == null ? ""
                    : cityOne.getSimplePinyin();
            String pyTwo = cityTwo.getSimplePinyin() == null ? ""
                    : cityTwo.getSimplePinyin();
            CollationKey first = (CollationKey) (cmp.getCollationKey(pyOne) == null ? ""
                    : cmp.getCollationKey(pyOne));
            CollationKey two = (CollationKey) (cmp.getCollationKey(pyTwo) == null ? ""
                    : cmp.getCollationKey(pyTwo));
            return cmp.compare(first.getSourceString(), two.getSourceString());
        }
        
    }
    
    /**
     * 
     * 将联系人进行排序<BR>
     * 1.移除没有display name的联系人
     *  2.按BaseContactComparator中规则对list进行排序
     * 
     * @param contactList List<BaseContactModel>
     */
    public static void sort(List<? extends City> contactList) {
        
        if (contactList != null) {
            BaseCityComparator comparator = new BaseCityComparator();
            try {
                Collections.sort(contactList, comparator);
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.e(TAG,
                        "Collections.sort failed, the msg is: "
                                + ex.getMessage());
            }
        }
    }
    
    /**
     * 
     * 将联系人列表转换为方便显示的列表<BR>
     * 1、将联系人按字母排序； 
     * 2、在以各字母开头的联系人中插入英文字母的String，
     * 形成BaseContactModel和String组成的list
     * 
     * @param cityList List<BaseContactModel>
     * @return 供显示使用的list
     */
    public static ArrayList<Object> contactListForDisplay(
            List<? extends City> cityList) {
        ArrayList<Object> contactListForDisplay = null;
        if (cityList != null) {
            
            // 排序
            sort(cityList);
            
            if (cityList.size() > 0) {
                contactListForDisplay = new ArrayList<Object>();
                // 将含“#”的放到最下面
                List<Object> bottomList = new ArrayList<Object>();
                for (City city : cityList) {
                    char initialLetter;
                    
                    // 如果display name为空，initial letter设置为“#”
                    if (StringUtil.isNullOrEmpty(city.getSimplePinyin())) {
                        initialLetter = '#';
                    }
                    
                    // 取display name对应拼音的首字母（需大写），并设置到name head letter.
                    else {
                        initialLetter = city.getSimplePinyin()
                                .toUpperCase()
                                .charAt(0);
                        if (initialLetter > 'Z' || initialLetter < 'A') {
                            initialLetter = '#';
                        }
                    }
                    city.setNameHeadLetter(String.valueOf(initialLetter));
                    if ("#".equals(city.getNameHeadLetter())) {
                        if (!bottomList.contains("#")) {
                            bottomList.add("#");
                        }
                        bottomList.add(city);
                    } else {
                        
                        // 如果contactListForDisplay中没有此拼音对应的首字母字符串，
                        //则添加该字符串到该list中
                        if (!contactListForDisplay.contains(city.getNameHeadLetter())) {
                            contactListForDisplay.add(city.getNameHeadLetter());
                        }
                        contactListForDisplay.add(city);
                    }
                }
                contactListForDisplay.addAll(bottomList);
            }
        }
        return contactListForDisplay;
    }
    

}
