/*
 * 文件名: DownloadMapAdapter.java
 * 版    权：  Copyright DingliCom Tech. Co. Ltd. All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 黄广府
 * 创建时间:2012-6-1
 * 
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap.logic.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.MKOLSearchRecord;
import com.baidu.mapapi.MKOLUpdateElement;
import com.baidu.mapapi.MKOfflineMap;
import com.dinglicom.walktourmap.R;
import com.dinglicom.walktourmap.logic.model.City;
import com.dinglicom.walktourmap.ui.OfflineMapActivity;

/**
 * [一句话功能简述]<BR>
 * [功能详细描述]
 * @author 黄广府
 * @version [WalkTour Client V100R001C03, 2012-6-1] 
 */
public class DownloadMapAdapter extends BaseAdapter {
    
    private List<MKOLSearchRecord> cityList;
    
    private MKOfflineMap mkOfflineMap;
    
    private Map<Integer, MKOLUpdateElement> updateElementHashMap;
    
    private Context context;
    
    public DownloadMapAdapter(Context context, MKOfflineMap mkOfflineMap,
            List<MKOLSearchRecord> cityList,Map<Integer, MKOLUpdateElement> updateElementHashMap ) {
        this.context = context;
        this.mkOfflineMap = mkOfflineMap;
        this.cityList = cityList;
        this.updateElementHashMap = updateElementHashMap;
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @return
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        if (cityList != null) {
            return cityList.size();
        }
        return 0;
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param position
     * @return
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return cityList.get(position);
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param position
     * @return
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {
        return cityList.get(position).cityID;
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param position
     * @param convertView
     * @param parent
     * @return
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MKOLSearchRecord city = cityList.get(position);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.offsearch_listitems, null);
            holder.cityName = (TextView) convertView.findViewById(R.id.itemName);
            holder.citySize = (TextView) convertView.findViewById(R.id.itemSize);
            holder.cityStatus = (TextView) convertView.findViewById(R.id.itemStatusText);
            holder.downBtn = (ImageButton) convertView.findViewById(R.id.Item_Button_off_search);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.cityName.setText(city.cityName);
        holder.citySize.setText(String.valueOf(city.size / 1048576)+"M");
        holder.cityStatus.setVisibility(View.GONE);
        if(updateElementHashMap.containsKey(city.cityID)){
            MKOLUpdateElement element = updateElementHashMap.get(city.cityID);
            if(element.status  == MKOLUpdateElement.DOWNLOADING){
                holder.cityStatus.setVisibility(View.VISIBLE);
                holder.downBtn.setEnabled(false);
                holder.cityStatus.setText(R.string.downloading);
            }else if(element.status  == MKOLUpdateElement.FINISHED){
                holder.cityStatus.setVisibility(View.VISIBLE);
                holder.downBtn.setEnabled(false);
                holder.cityStatus.setText(R.string.download_finished);
            }else if(element.status  == MKOLUpdateElement.WAITING){
                holder.cityStatus.setVisibility(View.VISIBLE);
                holder.downBtn.setEnabled(false);
                holder.cityStatus.setText(R.string.download_waitting);
            }
        }
        
        holder.downBtn.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                holder.cityStatus.setText(R.string.downloading);
                holder.cityStatus.setVisibility(View.VISIBLE);
                Toast.makeText(context,
                        String.format(context.getResources()
                                .getString(R.string.add_download_queue),
                                city.cityName),
                        Toast.LENGTH_LONG).show();
                v.setEnabled(false);
                Intent intent = new Intent(OfflineMapActivity.ADD_DOWNLOAD_TASK);
                City city2 = new City();
                city2.setId(city.cityID);
                city2.setName(city.cityName);
                city2.setSize(city.size);
                intent.putExtra("city", city2);
                //intent.putExtra("cityId", city.cityID);
                context.sendBroadcast(intent);
/*                if (mkOfflineMap.start(city.cityID)) {
                    Toast.makeText(context,
                            String.format(context.getResources()
                                    .getString(R.string.add_download_queue),
                                    city.cityName),
                            Toast.LENGTH_LONG).show();
                    v.setEnabled(false);
                    holder.cityStatus.setText(R.string.downloading);
                    holder.cityStatus.setVisibility(View.VISIBLE);
                }*/
            }
        });
        return convertView;
    }
    
    private static class ViewHolder {
        
        TextView cityName;
        
        TextView citySize;
        
        TextView cityStatus;
        
        ImageButton downBtn;
        
    }
}
