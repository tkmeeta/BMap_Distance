/*
 * 文件名: CityListAdapter.java
 * 版    权：  Copyright DingliCom Tech. Co. Ltd. All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 黄广府
 * 创建时间:2012-5-31
 * 
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap.logic.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.R.raw;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.sax.StartElementListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.MKOLUpdateElement;
import com.baidu.mapapi.MKOfflineMap;
import com.dinglicom.walktourmap.R;
import com.dinglicom.walktourmap.logic.model.City;
import com.dinglicom.walktourmap.utils.Match;

/**
 * [一句话功能简述]<BR>
 * [功能详细描述]
 * @author 黄广府
 * @version [WalkTour Client V100R001C03, 2012-5-31] 
 */
public class OfflineMapAdapter extends QuickAdapter {
    
    private Context context;
    
    //private List<City> citylList;
    
    private Map<Integer, MKOLUpdateElement> updateElementHashMap;
    
    private MKOfflineMap mkOfflineMap;
    
    public OfflineMapAdapter(Context context, List<City> cityList,Map<Integer, MKOLUpdateElement> updateElementHashMap,MKOfflineMap mkOfflineMap) {
        this.context = context;
        this.updateElementHashMap = updateElementHashMap;
        this.mkOfflineMap = mkOfflineMap;
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param position
     * @param convertView
     * @param parent
     * @return
     * @see com.dinglicom.walktourmap.logic.adapter.QuickAdapter#getItemView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final City city = (City)getDisplayList().get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LinearLayout.inflate(context,
                    R.layout.offlinemap_listitem_download,
                    null);
            holder.cityName = (TextView) convertView.findViewById(R.id.itemName);
            holder.citySize = (TextView) convertView.findViewById(R.id.itemSize);
            holder.cityStateTips = (TextView) convertView.findViewById(R.id.itemStateTips);
            holder.ratioProgressBar = (ProgressBar) convertView.findViewById(R.id.progressBar01);
            holder.ratioProgressBar.setVisibility(View.GONE);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.cityName.setText(city.getName());
        holder.citySize.setText(city.getSize()/1024/1024+ "M");
        if(updateElementHashMap.containsKey(city.getId())){
            MKOLUpdateElement element = updateElementHashMap.get(city.getId());
            if(element.status  == MKOLUpdateElement.DOWNLOADING){
                holder.cityStateTips.setVisibility(View.VISIBLE);
                holder.cityStateTips.setText(R.string.downloading);
            }else if(element.status  == MKOLUpdateElement.FINISHED){
                holder.cityStateTips.setVisibility(View.VISIBLE);
                holder.cityStateTips.setText(R.string.download_finished);
            }else if(element.status  == MKOLUpdateElement.WAITING){
                holder.cityStateTips.setVisibility(View.VISIBLE);
                holder.cityStateTips.setText(R.string.download_waitting);
            }
            holder.ratioProgressBar.setProgress(element.ratio);
            holder.ratioProgressBar.setVisibility(View.VISIBLE);
        }else {
            holder.cityStateTips.setVisibility(View.INVISIBLE);
            holder.ratioProgressBar.setVisibility(View.GONE);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context).setItems(new String[]{"下载","暂停","删除"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                mkOfflineMap.start(city.getId());
                                Toast.makeText(context, "开始下载："+ city.getName(), Toast.LENGTH_LONG).show();
                                break;
                            case 1:
                                mkOfflineMap.pause(city.getId());
                                Toast.makeText(context, "暂停下载："+ city.getName(), Toast.LENGTH_LONG).show();
                                break; 
                            case 2:
                                mkOfflineMap.remove(city.getId());
                                Toast.makeText(context, "删除下载："+ city.getName(), Toast.LENGTH_LONG).show();
                                break; 
                            default:
                                break;
                        }
                        
                    }
                } ).show();
            }
        });
        return convertView;
    }
    
    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @param obj
     * @param key
     * @return
     * @see com.dinglicom.walktourmap.logic.adapter.QuickAdapter#isMatched(java.lang.Object, java.lang.String)
     */
    @Override
    public boolean isMatched(Object obj, String key) {
        if (obj instanceof City) {
            // 可匹配通讯录中的姓名和简拼
            City city = (City) obj;
            List<String> values = new ArrayList<String>();
            
            values.add(city.getName());
            values.add(city.getSpellName());
            values.add(city.getSpellName());
            values.add(city.getInitialName());
            
            int size = values.size();
            String[] strs = new String[size];
            
            for (int i = 0; i < size; i++) {
                strs[i] = values.get(i);
            }
            return Match.match(key, strs);
        }
        return false;
    }
    
    public static class ViewHolder {
        
        TextView cityName;
        
        TextView citySize;
        
        TextView cityStateTips;
        
        ProgressBar ratioProgressBar;
    }
    
}
